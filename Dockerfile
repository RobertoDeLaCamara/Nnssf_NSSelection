# We specify the base image we need for our
# go application
FROM golang:latest

# Set go bin which doesn't appear to be set already.
#ENV GOBIN /go/bin

# build directories
RUN mkdir /app
RUN mkdir /go/src/app
ADD . /go/src/app
WORKDIR /go/src/app

# Go dep!
RUN go get -u github.com/golang/dep/...
RUN dep ensure

# Build my app
RUN go build -o /app/main .
CMD ["/app/main"]
ARG img_name=nssf-slice-selection-control
LABEL description="$img_name"

#Expose port
EXPOSE 1970


