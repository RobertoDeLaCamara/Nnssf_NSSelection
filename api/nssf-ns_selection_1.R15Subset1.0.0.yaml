openapi: 3.0.0
info:
  version: '1.R15Subset1.0.0'
  title: 'NSSF NS Selection'
  description: 'NSSF Network Slice Selection Service'
servers:
- url: https://{apiRoot}/nnssf-nsselection/v1
  variables:
    apiRoot:
      default: locahost:8080
paths:
  /network-slice-information:
    get:
      summary:  Retrieve the Network Slice Selection Information
      tags:
      - Network Slice Information (Document)
      operationId: Get
      parameters:
      - name: nf-type
        in: query
        description: NF type of the NF service consumer
        required: true
        schema:
          $ref: '#/components/schemas/NFType'
      - name: nf-id
        in: query
        description: NF Instance ID of the NF service consumer
        required: true
        schema:
          $ref: '#/components/schemas/NfInstanceId'
      - name: slice-info-request-for-registration
        in: query
        description: Requested network slice information during Registration procedure
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SliceInfoForRegistration'
      - name: tai
        in: query
        description: TAI of the UE
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Tai'
      responses:
        '200':
          description: OK (Successful Network Slice Selection)
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AuthorizedNetworkSliceInfo'
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProblemDetails'
        default:
          description: Unexpected error
          content:
            application/problem+json:
              schema:
                $ref: '#/components/schemas/ProblemDetails'

components:
  schemas:
    AuthorizedNetworkSliceInfo:
      type: object
      required:
      - allowedNssai
      properties:
        allowedNssai:
          $ref: '#/components/schemas/AllowedNssai'
        targetAmfSet:
          type: string

    SubscribedSNssai:
      type: object
      required:
      - subscribedSNssai
      properties:
        subscribedSNssai:
          $ref: '#/components/schemas/Snssai'
        defaultIndication:
          type: boolean

    SubscribedNssai:
      type: object
      required:
      - subscribedSNssai
      properties:
        subscribedSNssai:
          type: array
          items:
            $ref: '#/components/schemas/SubscribedSNssai'

    AllowedSNssai:
      type: object
      required:
      - allowedSNssai
      properties:
        allowedSNssai:
          $ref: '#/components/schemas/Snssai'

    AllowedNssai:
      type: object
      required:
      - allowedSNssai
      properties:
        allowedSNssai:
          type: array
          items:
            $ref: '#/components/schemas/AllowedSNssai'

    SliceInfoForRegistration:
      type: object
      required:
      - subscribedNSSAI
      properties:
        subscribedNSSAI:
          $ref: '#/components/schemas/SubscribedNssai'
        requestedNssai:
          type: array
          items:
            $ref: '#/components/schemas/Snssai'

    NFType:
      anyOf:
      - type: string
        enum:
        - NRF
        - UDM
        - AMF
        - SMF
        - AUSF
        - NEF
        - PCF
        - SMSF
        - NSSF
        - UDR
        - LMF
        - GMLC
        - 5G_EIR
        - SEPP
        - UPF
        - N3IWF
        - AF
        - UDSF
        - BSF
        - CHF
      - type: string
    NfInstanceId:
      type: string
      format: uuid
    PlmnId:
      type: object
      properties:
        mcc:
          $ref: '#/components/schemas/Mcc'
        mnc:
          $ref: '#/components/schemas/Mnc'
      required:
      - mcc
      - mnc
    Tai:
      type: object
      properties:
        plmnId:
          $ref: '#/components/schemas/PlmnId'
        tac:
          $ref: '#/components/schemas/Tac'
      required:
      - plmnId
      - tac
    Mcc:
      type: string
      pattern: '^\d{3}$'

    Mnc:
      type: string
      pattern: '^\d{2,3}$'

    Tac:
      type: string
      pattern: '(^[A-Fa-f0-9]{4}$)|(^[A-Fa-f0-9]{6}$)'

    Snssai:
      type: object
      properties:
        sst:
          type: '#/components/schemas/Uinteger'
          minimum: 0
          maximum: 255
        sd:
          type: string
      required:
      - sst
    Uinteger:
      type: integer
      minimum: 0
    Uri:
      type: string
    InvalidParam:
      type: object
      properties:
        param:
          type: string
        reason:
          type: string
      required:
      - param
    ProblemDetails:
      type: object
      properties:
        type:
          $ref: '#/components/schemas/Uri'
        title:
          type: string
        status:
          type: integer
        instance:
          $ref: '#/components/schemas/Uri'
        cause:
          type: string
        invalidParams:
          type: array
          items:
            $ref: '#/components/schemas/InvalidParam'
          minItems: 0

externalDocs:
  description: Documentation
  url: 'http://www.3gpp.org/ftp/Specs/archive/29_series/29.531/'
  