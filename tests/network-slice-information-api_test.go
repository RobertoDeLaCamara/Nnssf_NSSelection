/*
 *
 *  Copyright (c)   2018. Roberto de la Cámara (roberto.de.la.camara.garcia@gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 *
 */

package ut

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/RobertoDeLaCamara/Nnssf_NSSelection/server"
)

func TestNoQueryParameters(t *testing.T) {
	request := httptest.NewRequest("GET", "/network-slice-information/v1beta", nil)
	response := httptest.NewRecorder()
	router := server.NewRouter()
	router.ServeHTTP(response, request)
	if response.Code != http.StatusBadRequest {
		t.Errorf("Expected BadRequest, got %v", response.Code)
	}
}

func TestNotEnoughQueryParameters(t *testing.T) {
	request := httptest.NewRequest("GET", "/network-slice-information/v1beta?nf-type=AMF", nil)
	response := httptest.NewRecorder()
	router := server.NewRouter()
	router.ServeHTTP(response, request)
	if response.Code != http.StatusBadRequest {
		t.Errorf("Expected BadRequest, got %v", response.Code)
	}
}

func TestValidNFTypeAndNFId(t *testing.T) {
	tai := server.Tai{
		PlmnId: &server.PlmnId{
			Mcc: "505",
			Mnc: "280",
		},
		Tac: "0001",
	}
	data, _ := json.Marshal(tai)
	url := "/network-slice-information/v1beta?nf-type=AMF&nf-id=7af2abab-db52-4a69-bcc4-9019f8e6c857"
	request := httptest.NewRequest("GET", url+"&tai="+string(data), nil)
	response := httptest.NewRecorder()
	router := server.NewRouter()
	router.ServeHTTP(response, request)
	if response.Code != http.StatusOK {
		t.Errorf("Expected OK, got %v", response.Code)
	}
}

func TestNoTai(t *testing.T) {
	url := "/network-slice-information/v1beta?nf-type=AMF&nf-id=7af2abab-db52-4a69-bcc4-9019f8e6c857"
	request := httptest.NewRequest("GET", url, nil)
	response := httptest.NewRecorder()
	router := server.NewRouter()
	router.ServeHTTP(response, request)
	if response.Code != http.StatusBadRequest {
		t.Errorf("Expected OK, got %v", response.Code)
	}
}

func TestTai(t *testing.T) {
	tai := server.Tai{
		PlmnId: &server.PlmnId{
			Mcc: "505",
			Mnc: "280",
		},
		Tac: "0001",
	}
	data, _ := json.Marshal(tai)
	url := "/network-slice-information/v1beta?nf-type=AMF&nf-id=7af2abab-db52-4a69-bcc4-9019f8e6c857"
	request := httptest.NewRequest("GET", url+"&tai="+string(data), nil)
	response := httptest.NewRecorder()
	router := server.NewRouter()
	router.ServeHTTP(response, request)
	if response.Code != http.StatusOK {
		t.Errorf("Expected OK, got %v", response.Code)
	}

}

func TestSliceInfo4Reg(t *testing.T) {

	var req [2]server.Snssai
	var a uint32
	a = 0
	req[0] = server.Snssai{
		Sst: a,
		Sd:  "default",
	}
	a = 1
	req[1] = server.Snssai{
		Sst: a,
		Sd:  "one",
	}

	tai := server.Tai{
		PlmnId: &server.PlmnId{
			Mcc: "505",
			Mnc: "280",
		},
		Tac: "0001",
	}
	data1, _ := json.Marshal(tai)

	var subsSNssaiArray []server.SubscribedSNssai

	subsSNssaiArray = append(subsSNssaiArray, server.SubscribedSNssai{
		SubscribedSNssai:  &req[0],
		DefaultIndication: false,
	})

	sliceInfo4Reg := server.SliceInfoForRegistration{
		SubscribedNSSAI: &server.SubscribedNssai{
			SubscribedSNssai: subsSNssaiArray,
		},
		RequestedNssai: []server.Snssai{
			req[0],
		},
	}
	data2, _ := json.Marshal(sliceInfo4Reg)
	url := "/network-slice-information/v1beta?nf-type=AMF&nf-id=7af2abab-db52-4a69-bcc4-9019f8e6c857"
	request := httptest.NewRequest("GET", url+"&tai="+string(data1)+"&slice-info-request-for-registration="+string(data2), nil)
	response := httptest.NewRecorder()
	router := server.NewRouter()
	router.ServeHTTP(response, request)
	if response.Code != http.StatusOK {
		t.Errorf("Expected OK, got %v", response.Code)
	}

}
