/*
 *  Copyright (c)   2018. Roberto de la Cámara (roberto.de.la.camara.garcia@gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 *
 */

package ut

import (
	"testing"

	"gitlab.com/RobertoDeLaCamara/Nnssf_NSSelection/server"
)

func TestParseTrainingFile(*testing.T) {
	server.InitClassifier()
}

func TestClassify(t *testing.T) {
	type args struct {
		tai server.Tai
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
		{"URLLC", args{
			tai: server.Tai{
				PlmnId: &server.PlmnId{
					Mcc: "232",
					Mnc: "15",
				},
				Tac: "6602",
			},
		}, "URLLC", false},
	}
	cls := server.InitClassifier()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := server.Classify(tt.args.tai, cls)
			if (err != nil) != tt.wantErr {
				t.Errorf("Classify() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Classify() = %v, want %v", got, tt.want)
			}
		})
	}
}
