# Network Slice Selection Server

NSSF Network Slice Selection Service as per 3GPP TS 29.531 specification.

## Overview


### Running the server
To run the server, follow these simple steps:

```
go run nnssf-nsselection-server.go
```

