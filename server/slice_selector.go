/*
 *
 *  Copyright (c)   2018. Roberto de la Cámara (roberto.de.la.camara.garcia@gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 *
 *
 */

package server

import (
	"errors"
	"fmt"
	"log"
	"strconv"
)

//SelectSlice selects a network slice
/*
	Mandatory:
	1-nfType
	2-nfId
	Optional:
	3-slice-info-for-registration
	3.1 SubscribedNSSAI
	3.2 RequestedNssai
	4-Tai
	4.1 PlmnId
	4.2 Tac
*/
func SelectSlice(nfType string, nfID string, tai Tai, sliceInfo4Reg SliceInfoForRegistration) (*AuthorizedNetworkSliceInfo, error) {
	log.Printf("Selecting slice")
	if nfID != "" && nfType != "" {
		//TODO: Select slice
		//The goal is to select the SNSAAI within the Requested corresponding to the received TAI
		//TODO: If Requested NSSAI is not included in SubscribedNSSAI an error shall be returned
		//TODO: predict sd
		if (tai == Tai{}) {
			log.Printf("Null or invalid Tai, returning without calling classifier")
			return nil, errors.New("Invalid Tai")
		}
		//Initialize classifier
		cls := InitClassifier()
		//Classify slice
		sst, err := Classify(tai, cls)
		if err != nil {
			fmt.Println("error: ", err)
			return nil, err
		}
		sstUInt, _ := strconv.ParseUint(sst, 10, 64)
		var allowedSNssaiArray []AllowedSNssai
		var allowedNssai AllowedNssai
		allowedSNssaiArray = append(allowedSNssaiArray, AllowedSNssai{
			AllowedSNssai: &Snssai{
				Sst: uint32(sstUInt),
				//TODO: implement Sst classifier
				Sd: "default",
			},
		})
		allowedNssai.AllowedSNssai = allowedSNssaiArray
		response := AuthorizedNetworkSliceInfo{
			AllowedNssai: &allowedNssai,
			//TODO: implement TargetAMF selector function
			TargetAmfSet: "testAmf1",
		}
		return &response, nil

	}
	return nil, errors.New("NfType and NfID mandatory parameters are missing or are not valid")

}
