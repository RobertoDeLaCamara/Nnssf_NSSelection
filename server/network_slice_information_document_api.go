/*
 * Copyright (c) 2018.  Roberto de la Cámara (roberto.de.la.camara.garcia@gmail.com)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package server

import (
	"encoding/json"
	"net/http"
	"reflect"
	"regexp"
)

func Get(w http.ResponseWriter, r *http.Request) {

	//Query parameters:
	//1: nf-type
	nfType := r.URL.Query().Get("nf-type")
	if nfType != "" {
		isNfTypeValid, err := ValidateNfType(nfType)
		if err != nil || isNfTypeValid != true {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}
	} else {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	//2: nf-id
	nfId := r.URL.Query().Get("nf-id")
	if nfId != "" {
		isNfIdValid, err := ValidateNfId(nfId)
		if err != nil || isNfIdValid != true {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}
	} else {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	//3: Tai
	taiString := r.URL.Query().Get("tai")
	tai := Tai{}
	if taiString != "" {
		err := json.Unmarshal([]byte(taiString), &tai)
		isTaiValid, err := ValidateTai(tai)
		if err != nil || isTaiValid != true {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}
	}
	//4: SliceInfoForRegistration
	sliceInfo4RegString := r.URL.Query().Get("slice-info-request-for-registration")
	var sliceInfo4Reg SliceInfoForRegistration
	if sliceInfo4RegString != "" {
		err := json.Unmarshal([]byte(sliceInfo4RegString), &sliceInfo4Reg)
		isSliceInfo4RegValid, err := ValidateSliceInfo4Reg(sliceInfo4Reg)
		if err != nil || isSliceInfo4RegValid != true {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}
	}
	//Build response
	response, err := SelectSlice(nfType, nfId, tai, sliceInfo4Reg)
	if err == nil {
		encoder := json.NewEncoder(w)
		encoder.Encode(&response)
		//Set Response code
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		return
	} else {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

}

func ValidateNfType(nfType string) (bool, error) {

	if nfType == "" {
		return false, nil
	}

	valid := map[string]bool{
		"NRF":    true,
		"UDM":    true,
		"AMF":    true,
		"SMF":    true,
		"AUSF":   true,
		"NEF":    true,
		"PCF":    true,
		"SMSF":   true,
		"NSSF":   true,
		"UDR":    true,
		"LMF":    true,
		"GMLC":   true,
		"5G_EIR": true,
		"SEPP":   true,
		"UPF":    true,
		"N3IWF":  true,
		"AF":     true,
		"UDSF":   true,
		"BSF":    true,
		"CHF":    true,
	}
	return valid[nfType], nil

}

func ValidateNfId(nfId string) (bool, error) {

	r := regexp.MustCompile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$")
	return r.MatchString(nfId), nil
}

func ValidateTai(tai Tai) (bool, error) {

	r := regexp.MustCompile("(^[A-Fa-f0-9]{4}$)|(^[A-Fa-f0-9]{6}$)")
	s := regexp.MustCompile("^\\d{2,3}$")
	t := regexp.MustCompile("^\\d{3}$")
	return r.MatchString(tai.Tac) && s.MatchString(tai.PlmnId.Mnc) && t.MatchString(tai.PlmnId.Mcc), nil
}

func ValidateSliceInfo4Reg(sliceInfo SliceInfoForRegistration) (bool, error) {

	for _, reqNssai := range sliceInfo.RequestedNssai {
		var a uint32
		var b string
		if reflect.TypeOf(reqNssai.Sst) != reflect.TypeOf(a) {
			return false, nil
		}
		if reflect.TypeOf(reqNssai.Sd) != reflect.TypeOf(b) {
			return false, nil
		}

	}
	for _, subsSNssai := range sliceInfo.SubscribedNSSAI.SubscribedSNssai {
		var a bool
		var b uint32
		var c string

		if reflect.TypeOf(subsSNssai.SubscribedSNssai.Sst) != reflect.TypeOf(b) {
			return false, nil
		}
		if reflect.TypeOf(subsSNssai.SubscribedSNssai.Sd) != reflect.TypeOf(c) {
			return false, nil
		}

		if reflect.TypeOf(subsSNssai.DefaultIndication) != reflect.TypeOf(a) {
			return false, nil
		}

	}
	return true, nil
}
