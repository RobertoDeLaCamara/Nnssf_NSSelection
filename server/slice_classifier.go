/*
 *
 *  Copyright (c)   2018. Roberto de la Cámara (roberto.de.la.camara.garcia@gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 *
 */

package server

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/n3integration/classifier/naive"
)

//InitClassifier reads training data from data file
//and trains a classifier
//with that data.
func InitClassifier() *naive.Classifier {
	//Parsing training data file
	log.Printf("Loading training file...")
	//Create classifier and train it
	cls := naive.New()
	//Train classifier with training data from file
	log.Printf("Training classsifier...")
	m := ReadCsvFile("../data/sst_training_data.csv")
	for index, value := range m {
		if index != 0 {
			//fmt.Println("Tai:", value.tai)
			//fmt.Println("Sst:", value.sst)
			cls.TrainString(value.tai, value.sst)
		}

	}
	return cls
}

//Classify predicts sst based on PlamnId+TAC
//slice selection can be seen as a classification problem for sst based on TAI (PlmnId and Tac) input variables
//TODO: predict sd
func Classify(tai Tai, cls *naive.Classifier) (string, error) {
	log.Printf("Classifying...")
	//Get input variables
	mcc := tai.PlmnId.Mcc
	mnc := tai.PlmnId.Mnc
	tac := tai.Tac
	//Classify
	if pred, err := cls.ClassifyString(mcc + mnc + tac); err == nil {
		fmt.Println("Classification => ", pred)
		return pred, nil
	} else {
		fmt.Println("error: ", err)
		return "", err
	}

}

//Pair represents the Ssst-tai pair parsed from the data file
type Pair struct {
	sst string
	tai string
}

//ReadCsvFile parses a CSV data file
func ReadCsvFile(filePath string) []Pair {
	// Load a csv file.
	f, _ := os.Open(filePath)
	// Create a new reader.
	r := csv.NewReader(f)
	//Initialize pair array
	m := []Pair{}
	j := 0
	for {
		record, err := r.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}

		if err != nil {
			panic(err)
		}
		// Display record.
		// ... Display record length.
		// ... Display all individual elements of the slice.
		//fmt.Println(record)
		//fmt.Println(len(record))
		sst := record[3]
		tai := record[0] + record[1] + record[2]
		m = append(m, Pair{sst, tai})
		j++
	}
	return m
}
