# Nnssf_NSSelection

Machine learning-based 5G NSSF Network Slice Selection Service as per 3GPP TS 29.531.  



Slice selection can be modeled as a [classification problem](https://en.wikipedia.org/wiki/Statistical_classification):

* Returned AllowedNSSAI is selected using PlmnId and TAC as input variables
* A [Naive Bayes](https://en.wikipedia.org/wiki/Naive_Bayes_classifier) classifier is used for AllowedNSSAI selection
* No rule-based decisions and no Policy Engine needed
* The classifier requires a Training Phase which can be seen as equivalent to Network Slice Profile(which includes the AllowedNSSAI) provisioning in rule-based Slice Selectors  
